#-------------------------------------------------
#
# Project created by QtCreator 2018-06-14T08:34:49
#
#-------------------------------------------------

QT       += core gui uitools

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = EnthalpyPlotter
TEMPLATE = app
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
INCLUDEPATH += $$PWD/include/

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    Exceptions/settingsexception.cpp \
    Lib/qcustomplot.cpp \
    sources/contentiterator.cpp \
    sources/reader.cpp \
    sources/stringdata.cpp \
    sources/variantdata.cpp \
    sources/writer.cpp \
    main.cpp \
    mainwindow.cpp \
    csvmodel.cpp \
    settingsmanager.cpp \
    tableshow.cpp \
    plotconfig.cpp

HEADERS += \
    Exceptions/settingsexception.h \
    include/qtcsv/abstractdata.h \
    include/qtcsv/qtcsv_global.h \
    include/qtcsv/reader.h \
    include/qtcsv/stringdata.h \
    include/qtcsv/variantdata.h \
    include/qtcsv/writer.h \
    Lib/qcustomplot.h \
    sources/contentiterator.h \
    sources/filechecker.h \
    sources/symbols.h \
    mainwindow.h \
    csvmodel.h \
    settingsmanager.h \
    tableshow.h \
    plotconfig.h

FORMS += \
    mainwindow.ui \
    tableshow.ui \
    plotconfig.ui
