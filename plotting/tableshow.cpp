#include "tableshow.h"
#include "ui_tableshow.h"
#include <QCheckBox>

TableShow::TableShow(CSVModel *model, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TableShow)
{
    ui->setupUi(this);

    this->model = model;
    int k= 0, inner= 0, outer= 0;
    for (auto i: model->getCSVData()) {
        QLabel *label = new QLabel;
        label->setText(model->getCSVLabels()[k]);
        ui->mainLayout->addWidget(label);
        for (auto j: i[0]) {
            QCheckBox *box = new QCheckBox(j);
            box->setObjectName(QString::number(outer)+"-0-"+QString::number(inner));
            box->setParent(ui->scrollArea);
            ui->mainLayout->addWidget(box);
            inner++;
        }
        inner= 0;
        k++;
        outer++;
    }
}

TableShow::~TableShow()
{
    this->model = nullptr;
    delete ui;
}

void TableShow::on_applyButton_clicked()
{
    QList<QCheckBox *> widgets = ui->scrollArea->findChildren<QCheckBox *>();
        QVector<QList<QStringList>> csvDataToShow = QVector<QList<QStringList>>();
        for(auto box: widgets){
            if (box->isChecked()) {
                QList<QStringList> data = QList<QStringList>();
                auto indexList = box->objectName().split('-');
                int outer= indexList[0].toInt(), inner= indexList[2].toInt();
                for (auto item: this->model->getCSVData()[outer]) {
                    QStringList list = QStringList();
                    list.append(item[inner]);
                    data.append(list);
                }
                csvDataToShow.append(data);
            }
        }
        this->model->setCSVData(csvDataToShow);
        close();
}
