#ifndef SETTINGSEXCEPTION_H
#define SETTINGSEXCEPTION_H

#include <iostream>
#include <exception>

class SettingsException : public std::runtime_error {
public:
    SettingsException(std::string msg) : runtime_error(msg.c_str()) {
    }
};

#endif // SETTINGSEXCEPTION_H
