#include "plotconfig.h"
#include "ui_plotconfig.h"
#include "mainwindow.h"
#include <QMessageBox>

PlotConfig::PlotConfig(int high, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlotConfig)
{
    ui->setupUi(this);
    ui->highIndex->setMaximum(high);
    ui->lowIndex->setMaximum(high);
}

PlotConfig::~PlotConfig()
{
    delete ui;
}

void PlotConfig::on_plotButton_clicked()
{
    int low, high;
    low = ui->lowIndex->value(); high = ui->highIndex->value();
    bool chart = ui->power->isChecked() || ui->enthalpy->isChecked();
    if (low>= high) {
        QMessageBox::critical(this,
                              "Error", "Wrong index.",
                              QMessageBox::Ok);
        return;
    } else if (!chart) {
        QMessageBox::critical(this,
                              "Error", "Select plot.",
                              QMessageBox::Ok);
        return;
    }
    if (high- low>= 500)
        QMessageBox::information(this,
                              "Info", "Plotting may take some time.",
                              QMessageBox::Ok);
    emit setRange(low, high);
    if (ui->power->isChecked())
        emit plot(1);
    else
        emit plot(0);
    close();
}
