#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QSettings>
#include <QString>
#include <QApplication>
#include <QFileDialog>
#include "Exceptions/settingsexception.h"

class SettingsManager
{
public:
    SettingsManager(QString f = QApplication::applicationDirPath() + "/settings.ini");
    void load();
    void save(QString key, QString value);
    QSettings *getSettings();
private:
    const QString filename;
    QSettings *settings;
    QMap<QString, QString> data;
};

#endif // SETTINGSMANAGER_H
