#include "csvmodel.h"
#include "qtcsv/reader.h"

CSVModel::CSVModel(QSettings *settings, QObject *parent): QAbstractTableModel(parent)
{
    QString path = settings->value("measurement_path").toString();
    settings->beginGroup("files");
    for(auto i: settings->allKeys()){
        QList<QStringList> readData = QtCSV::Reader::readToList(path + "/" + settings->value(i).toString(), ",");
        this->csvLabels.push_back(settings->value(i).toString());
        //some metadata not requied in tableview
        for (int i = 0; i < 6; ++i){
            readData.removeFirst();
        }
        this->csvData.push_back(readData);
    }
    settings->endGroup();
}

int CSVModel::rowCount(const QModelIndex & /*parent*/) const
{
    if(this->csvDataToShow.size())
        return this->csvDataToShow[0].size();
    else
        return 0;
}

int CSVModel::columnCount(const QModelIndex & /*parent*/) const
{
    if(this->csvDataToShow.size())
        return this->csvDataToShow.size();//this->csvDataToShow[0][0].size()*
    else
        return 0;
}

QVariant CSVModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        return QString(this->csvDataToShow[index.column()][index.row()+ 1][0]);//index.column()% 6
    }
    return QVariant();
}

QVariant CSVModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            //6 -> CSV collumns count
            return this->csvDataToShow[section][0][0];
        }
    }
    return QVariant();
}

void CSVModel::updateModel()
{
    beginResetModel();
//    myData.clear();
    endResetModel();
//    QModelIndex topLeft = createIndex(0,0);
//    QModelIndex bottomRight = createIndex(this->csvDataToShow[0].size(), this->csvDataToShow.size());
//    emit dataChanged(topLeft, bottomRight);
}

QVector<QList<QStringList> > CSVModel::getCSVData() const
{
    return this->csvData;
}

QVector<QString> CSVModel::getCSVLabels() const
{
    return this->csvLabels;
}

void CSVModel::setCSVData(QVector<QList<QStringList>> data)
{
    this->csvDataToShow= data;
}
