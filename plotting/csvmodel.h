#ifndef CSVMODEL_H
#define CSVMODEL_H

#include <QAbstractTableModel>
#include <QSettings>

class CSVModel : public QAbstractTableModel
{
public:
    CSVModel(QSettings *settings, QObject *parent = 0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    void updateModel();
    QVector<QList<QStringList>> getCSVData() const;
    QVector<QString> getCSVLabels() const;
    void setCSVData(QVector<QList<QStringList>> data);
private:
    QVector<QList<QStringList>> csvData;
    QVector<QList<QStringList>> csvDataToShow;
    QVector<QString> csvLabels;
};

#endif // CSVMODEL_H
