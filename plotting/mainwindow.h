#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "settingsmanager.h"
#include "csvmodel.h"
#include "Lib/qcustomplot.h"
#include "plotconfig.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    void drawEnthalpy();
    void drawPower();

    int low, high;
    SettingsManager settings;
    CSVModel *model;
    bool error;
    Ui::MainWindow *ui;
    QVector<double> date;


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void setRange(int low, int high);
    void selectPlot(int p);

    void graphClicked(QCPAbstractPlottable *plottable, int dataIndex);
    void moveLegend();
    void removeAllGraphs();
    void contextMenuRequest(QPoint pos);

    void on_actionOpen_triggered();
    void on_actionSelect_rows_triggered();
    void on_actionLegend_off_triggered(bool checked);
    void on_actionProperties_triggered();

    void on_actionSave_triggered();
};

#endif // MAINWINDOW_H
