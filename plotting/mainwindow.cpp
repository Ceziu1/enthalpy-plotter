#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileInfo"
#include "tableshow.h"
#include <QtMath>

bool fileExist(QString path){
    QFileInfo checkFile(path);
    if (checkFile.exists() && checkFile.isFile()) {
        return true;
    }
    return false;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString dir;
    try{
        settings.load();
        dir = settings.getSettings()->value("measurement_path").toString();
    } catch (SettingsException &e){
        QMessageBox::information(this, "Warning", e.what(), QMessageBox::Ok);
        dir = QFileDialog::getExistingDirectory(this,
                                                tr("Open Directory"),
                                                "/Users",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
        settings.save("measurement_path", dir);
    }
    settings.getSettings()->beginGroup("files");
    for(auto i: settings.getSettings()->allKeys()){
        if (!fileExist(dir + "/" + settings.getSettings()->value(i).toString())){
            QMessageBox::critical(this,
                                  "Error", "File " + settings.getSettings()->value(i).toString() + " does not exist in this path. Open valid directory.",
                                  QMessageBox::Ok);
            this->error = true;
            break;
        }
        this->error = false;
    }

    settings.getSettings()->endGroup();

    if (!this->error) {
        this->model = new CSVModel(settings.getSettings());
        ui->tableView->setModel(this->model);
        ui->tableView->show();
    }

    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes |
                                  QCP::iSelectLegend | QCP::iSelectPlottables);

    ui->customPlot->plotLayout()->insertRow(0);
    QCPTextElement *title = new QCPTextElement(ui->customPlot, "Enthalpy Charts", QFont("sans", 17, QFont::Bold));
    ui->customPlot->plotLayout()->addElement(0, 0, title);

    ui->customPlot->yAxis->setLabel("Enthalpy [kJ/kg]");
    ui->customPlot->xAxis->setLabel("Date");
    ui->customPlot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    ui->customPlot->legend->setFont(legendFont);
    ui->customPlot->legend->setSelectedFont(legendFont);
    ui->customPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    ui->customPlot->rescaleAxes();

    ui->customPlot->xAxis->setTickLabelFont(QFont(QFont().family(), 8));
    ui->customPlot->xAxis->setTickLabelFont(QFont(QFont().family(), 8));

    // connect slot that shows a message in the status bar when a graph is clicked:
    connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,int,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*,int)));

    // setup policy and connect slot for context menu popup:
    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::graphClicked(QCPAbstractPlottable *plottable, int dataIndex)
{
  // since we know we only have QCPGraphs in the plot, we can immediately access interface1D()
  // usually it's better to first check whether interface1D() returns non-zero, and only then use it.
    double dataValue = plottable->interface1D()->dataMainValue(dataIndex);
    QString message = QString("Clicked on graph '%1' at data point #%2 with value %3.").arg(plottable->name()).arg(dataIndex).arg(dataValue);
    ui->statusBar->showMessage(message, 5000);
}

void MainWindow::removeAllGraphs()
{
  ui->customPlot->clearGraphs();
  ui->customPlot->replot();
}

void MainWindow::contextMenuRequest(QPoint pos)
{
  QMenu *menu = new QMenu(this);
  menu->setAttribute(Qt::WA_DeleteOnClose);

  if (ui->customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
  {
    menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
    menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
    menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
    menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
    menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
  }
  menu->popup(ui->customPlot->mapToGlobal(pos));
}

double calculateTemperature(double TD){
    if(TD >= 0)//temperatura
        return 22120000 * (qExp(((-7.691234564 * (1 - (273.15 + TD) / 647.3) - 26.08023696 * qPow((1 - (273.15 + TD) / 647.3), 2) - 168.1706546 * qPow((1 - (273.15 + TD) / 647.3), 3) + 64.23285504 * qPow((1 - (273.15 + TD) / 647.3), 4) - 118.9646225 * qPow((1 - (273.15 + TD) / 647.3), 5)) / ((273.15 + TD) / 647.3) / (1 + 4.16711732 * (1 - (273.15 + TD) / 647.3) + 20.9750676 * qPow((1 - (273.15 + TD) / 647.3),2)) - (1 - (273.15 + TD) / 647.3) / (1000000000 * qPow((1 - (273.15 + TD) / 647.3), 2) + 6))));
    else
        return 100 * qExp(-20.947 * (273.15 / (273.15 + TD) - 1) - 3.56654 * qLn(273.15 / (273.15 + TD)) + 2.01889 * (1 - (273.15 + TD) / 273.15) + qLn(6.10714));
}

void MainWindow::drawEnthalpy()
{
    this->date.clear();
    QVector<double> enthalpyInput1, enthalpyInput2, enthalpyOutput1, enthalpyOutput2;
    double temperature, tempPointer, humidity;
    const double PT = 101325.262;
    QString format = "yyyy-MM-ddTHH:mm:ss";
    QDateTime a;
    for (int i = this->low; i < this->high; ++i) {
        tempPointer = calculateTemperature(this->model->getCSVData()[3][i].back().toDouble());
        temperature = this->model->getCSVData()[3][i].back().toDouble();
        humidity = this->model->getCSVData()[8][i].back().toDouble();
        double v = ((humidity / 100 ) * tempPointer)/100;
        double xVal = 0.6220135 * (v *100) / (PT - v *100);
        double cpVal = 1000 * ((1.0055 + 0.000000135 * qPow((temperature + 30),2)) + (1.8584 + 0.00009404 * temperature + 0.000000373 * qPow((temperature),2)) * xVal) / (1 + xVal);
        enthalpyInput1.push_back((cpVal * temperature + (2500000 - temperature * 2593) * xVal / (1 + xVal))/ 1000);
        a = QDateTime::fromString(this->model->getCSVData()[0][i][4], format);
        this->date.push_back(a.toTime_t());

        tempPointer = calculateTemperature(this->model->getCSVData()[5][i].back().toDouble());
        temperature = this->model->getCSVData()[5][i].back().toDouble();
        humidity = this->model->getCSVData()[10][i].back().toDouble();
        v = ((humidity / 100 ) * tempPointer)/100;
        xVal = 0.6220135 * (v *100) / (PT - v *100);
        cpVal = 1000 * ((1.0055 + 0.000000135 * qPow((temperature + 30),2)) + (1.8584 + 0.00009404 * temperature + 0.000000373 * qPow((temperature),2)) * xVal) / (1 + xVal);
        enthalpyInput2.push_back((cpVal * temperature + (2500000 - temperature * 2593) * xVal / (1 + xVal))/ 1000);

        tempPointer = calculateTemperature(this->model->getCSVData()[4][i].back().toDouble());
        temperature = this->model->getCSVData()[4][i].back().toDouble();
        humidity = this->model->getCSVData()[9][i].back().toDouble();
        v = ((humidity / 100 ) * tempPointer)/100;
        xVal = 0.6220135 * (v *100) / (PT - v *100);
        cpVal = 1000 * ((1.0055 + 0.000000135 * qPow((temperature + 30),2)) + (1.8584 + 0.00009404 * temperature + 0.000000373 * qPow((temperature),2)) * xVal) / (1 + xVal);
        enthalpyOutput1.push_back((cpVal * temperature + (2500000 - temperature * 2593) * xVal / (1 + xVal))/ 1000);

        tempPointer = calculateTemperature(this->model->getCSVData()[6][i].back().toDouble());
        temperature = this->model->getCSVData()[6][i].back().toDouble();
        humidity = this->model->getCSVData()[11][i].back().toDouble();
        v = ((humidity / 100 ) * tempPointer)/100;
        xVal = 0.6220135 * (v *100) / (PT - v *100);
        cpVal = 1000 * ((1.0055 + 0.000000135 * qPow((temperature + 30),2)) + (1.8584 + 0.00009404 * temperature + 0.000000373 * qPow((temperature),2)) * xVal) / (1 + xVal);
        enthalpyOutput2.push_back((cpVal * temperature + (2500000 - temperature * 2593) * xVal / (1 + xVal))/ 1000);
    }
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
    dateTicker->setDateTimeFormat(format);
    ui->customPlot->xAxis->setTicker(dateTicker);
    ui->customPlot->xAxis->setRange(this->date.first(), this->date.back());

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("Input enthalpy #1").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(this->date, enthalpyInput1);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    QPen graphPen;
    graphPen.setColor(QColor(119,209,249));
    graphPen.setWidthF(2);
    ui->customPlot->graph()->setPen(graphPen);

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("Input enthalpy #2").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(this->date, enthalpyInput2);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    graphPen.setColor(QColor(23,178,215));
    graphPen.setWidthF(2);
    ui->customPlot->graph()->setPen(graphPen);

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("Output enthalpy #1").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(this->date, enthalpyOutput1);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    graphPen.setColor(QColor(252,98,93));
    graphPen.setWidthF(2);
    ui->customPlot->graph()->setPen(graphPen);

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("Output enthalpy #2").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(this->date, enthalpyOutput2);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsStepCenter);
    graphPen.setColor(QColor(237, 67, 54));
    graphPen.setWidthF(2);
    ui->customPlot->graph()->setPen(graphPen);
    //temp#1 wlot -> 3
    //wilg#1 wlot -> 8

    //temp#2 wlot -> 5
    //wilg#2 wlot -> 10

    //temp#1 wylot -> 4
    //wilg#1 wylot -> 9

    //temp#2 wylot -> 6
    //wilg#2 wylot -> 11
}

void MainWindow::drawPower()
{
    ;
}

void MainWindow::setRange(int low, int high)
{
    this->low = low;
    this->high = high;
}

void MainWindow::selectPlot(int p)
{
    ui->customPlot->clearGraphs();
    if(!p)
        drawEnthalpy();
    else
        drawPower();
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();
}

void MainWindow::moveLegend()
{
  if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
  {
    bool ok;
    int dataInt = contextAction->data().toInt(&ok);
    if (ok)
    {
      ui->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
      ui->customPlot->replot();
    }
  }
}

void MainWindow::on_actionOpen_triggered()
{
    auto dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "/Users",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    settings.save("measurement_path", dir);
    settings.getSettings()->beginGroup("files");
    for(auto i: settings.getSettings()->allKeys()){
        if (!fileExist(dir + "/" + settings.getSettings()->value(i).toString())){
            QMessageBox::critical(this,
                                  "Error", "File " + settings.getSettings()->value(i).toString() + " does not exist in this path. Open valid directory.",
                                  QMessageBox::Ok);
            this->error = true;
            break;
        }
        this->error = false;
    }
    settings.getSettings()->endGroup();
    if (!this->error) {
        this->model = new CSVModel(settings.getSettings());
        ui->tableView->setModel(this->model);
        ui->tableView->show();
    }
}

void MainWindow::on_actionSelect_rows_triggered()
{
    if (this->error) {
        QMessageBox::critical(this,
                              "Error", "Open valid directory.",
                              QMessageBox::Ok);
        return;
    }
    TableShow tableShow(model);
    tableShow.setModal(true);
    tableShow.exec();
    this->model->updateModel();
}

void MainWindow::on_actionLegend_off_triggered(bool checked)
{
    checked ? ui->customPlot->legend->setVisible(false) : ui->customPlot->legend->setVisible(true);
    ui->customPlot->replot();
}

void MainWindow::on_actionProperties_triggered()
{
    PlotConfig plot(this->model->getCSVData()[0].size(), this);
    connect(&plot, SIGNAL(setRange(int, int)), this, SLOT(setRange(int,int)));
    connect(&plot, SIGNAL(plot(int)), this, SLOT(selectPlot(int)));
    plot.setModal(true);
    plot.exec();
}

void MainWindow::on_actionSave_triggered()
{
    auto dir = QFileDialog::getSaveFileName(this, tr("Save plot"), "plot1.pdf", tr("All Files (*)"));
    ui->customPlot->savePdf(dir);
}
