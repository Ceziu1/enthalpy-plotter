#ifndef TABLESHOW_H
#define TABLESHOW_H

#include <QDialog>
#include "csvmodel.h"

namespace Ui {
class TableShow;
}

class TableShow : public QDialog
{
    Q_OBJECT

public:
    explicit TableShow(CSVModel *model, QWidget *parent = 0);
    ~TableShow();

private slots:
    void on_applyButton_clicked();

private:
    Ui::TableShow *ui;
    CSVModel* model;
};

#endif // TABLESHOW_H
