#include "settingsmanager.h"

SettingsManager::SettingsManager(QString f) : filename(f)
{
    this->settings = new QSettings(this->filename, QSettings::NativeFormat);
    this->data.insert("Czerpnia wewnętrzna #1", "Otwarcie_czerpnia_GWC_wew.csv");
    this->data.insert("Czerpnia zewnętrzna #2", "Otwarcie_czerpnia_GWC_zew.csv");
    this->data.insert("Czerpnia ścienna", "Otwarcie_czerpnia_scienna.csv");
    this->data.insert("Temperatura czerpnia #1 wlot", "Temp_czerpnia_GWC_wew_wlot.csv");
    this->data.insert("Temperatura czerpnia #1 wylot", "Temp_czerpnia_GWC_wew_wylot.csv");
    this->data.insert("Temperatura czerpnia #2 wlot", "Temp_czerpnia_GWC_zew_wlot.csv");
    this->data.insert("Temperatura czerpnia #2 wylot", "Temp_czerpnia_GWC_zew_wylot.csv");
    this->data.insert("Temperatura czerpnia ścienna", "Temp_czerpnia_scienna.csv");
    this->data.insert("Wilgotność czerpnia #1 wlot", "Wilg_czerpnia_GWC_wew_wlot.csv");
    this->data.insert("Wilgotność czerpnia #1 wylot", "Wilg_czerpnia_GWC_wew_wylot.csv");
    this->data.insert("Wilgotność czerpnia #2 wlot", "Wilg_czerpnia_GWC_zew_wlot.csv");
    this->data.insert("Wilgotność czerpnia #2 wylot", "Wilg_czerpnia_GWC_zew_wylot.csv");
}

void SettingsManager::load()
{
    QString key = this->settings->value("measurement_path", "").toString();
    if (key == ""){
        this->settings->beginGroup("files");
        foreach (auto i, this->data.keys()) {
            this->save(i, this->data.value(i));
        }
        this->settings->endGroup();
        throw SettingsException("Config file not found. Please select data folder.");
    }
}

//Path to directory
//Filenames - 12 items
void SettingsManager::save(QString key, QString value)
{
    this->settings->setValue(key, value);
}

QSettings *SettingsManager::getSettings()
{
    return this->settings;
}
