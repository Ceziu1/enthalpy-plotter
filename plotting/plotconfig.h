#ifndef PLOTCONFIG_H
#define PLOTCONFIG_H

#include <QDialog>

namespace Ui {
class PlotConfig;
}

class PlotConfig : public QDialog
{
    Q_OBJECT

private:
    Ui::PlotConfig *ui;


public:
    explicit PlotConfig(int high, QWidget *parent = 0);
    enum Plot{
        Enthalpy,
        Power
    };
    ~PlotConfig();

signals:
    void setRange(int low, int high);
    void plot(int p);

private slots:
    void on_plotButton_clicked();

};

#endif // PLOTCONFIG_H
