import csv, sys, os, glob, datetime
from builtins import FileExistsError

def isDate(timestamp):
  try:
    format = "%Y-%m-%dT%H:%M:%S"
    date = datetime.datetime.strptime(timestamp, format)
    return True
  except ValueError:
    return False

def getDate(timestamp):
  format = "%Y-%m-%dT%H:%M:%S"
  return datetime.datetime.strptime(timestamp, format)

def parseCSV(filename):
  delimiter = ";"
  with open(filename, 'r', encoding='utf-8') as csvfile:
    reader = csv.reader(csvfile, delimiter=delimiter)
    for row in reader:
      yield row

if __name__ == "__main__":
  try:
    toCheck = []
    toSave = []
    filename = sys.argv[1]
    filesList = glob.glob('*.csv')
    try:
      os.mkdir("output")
    except FileExistsError:
      pass
    for i in filesList:
      toCheck.append(parseCSV(i))
      toSave.append(open('output/'+i, 'w'))
    for c, s in zip(toCheck, toSave):
      writer = csv.writer(s, dialect='excel')
      #c-> csv , s-> nowy
      generated = parseCSV(filename)
      for i in generated:
        #każda linia pliku bazowego
        if len(i) == 6 and isDate(i[4]):
            #pomiar co 5 minut
          for k in c:#k-> linia w pliku csv
            try:
              if len(k) == 6 and isDate(k[4]):
                if getDate(i[4]) > getDate(k[4]):
                  if getDate(i[4]) - getDate(k[4]) < datetime.timedelta(seconds=5):
                    writer.writerow(k)
                    break
                elif getDate(k[4]) - getDate(i[4]) < datetime.timedelta(seconds=5):
                  writer.writerow(k)
                  break
              # właściwe nagłówki
              elif '#' in k[0]:
                writer.writerow(k)
            except IndexError:
              continue
  except IndexError:
    print("No filename specified")
    exit(1)
